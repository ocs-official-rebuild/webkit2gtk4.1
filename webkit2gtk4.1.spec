%global add_to_license_files() \
        mkdir -p _license_files ; \
        cp -p %1 _license_files/$(echo '%1' | sed -e 's!/!.!g')

%global __provides_exclude_from ^(%{_libdir}/webkit2gtk-4\\.0/.*\\.so|%{_libdir}/webkit2gtk-4\\.1/.*\\.so|%{_libdir}/webkitgtk-6\\.0/.*\\.so)$
%global upstream_name webkitgtk

%bcond_with docs
%bcond_with gamepad

Summary:        WebKitGTK for GTK 3 and libsoup 3
Name:           webkit2gtk4.1
Version:        2.41.6
Release:        9%{?dist}
License:        LGPLv2
URL:            https://www.webkitgtk.org/
Source0:        https://webkitgtk.org/releases/%{upstream_name}-%{version}.tar.xz

BuildRequires:  bison cmake flex gcc-c++ gettext git gperf ninja-build
BuildRequires:  bubblewrap hyphen-devel libatomic python3 xdg-dbus-proxy
BuildRequires:  perl(English) perl(FindBin) perl(JSON::PP)
BuildRequires:  ruby rubygems rubygem-json
BuildRequires:  pkgconfig(atspi-2) pkgconfig(cairo) pkgconfig(egl)
BuildRequires:  pkgconfig(enchant-2) pkgconfig(fontconfig)
BuildRequires:  pkgconfig(freetype2) pkgconfig(gl) pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(glesv2) pkgconfig(gobject-introspection-1.0)
BuildRequires:  pkgconfig(gstreamer-1.0) pkgconfig(gstreamer-plugins-base-1.0)
BuildRequires:  pkgconfig(gtk+-3.0) pkgconfig(harfbuzz) pkgconfig(icu-uc)
BuildRequires:  pkgconfig(lcms2) pkgconfig(libgcrypt) pkgconfig(libjpeg)
BuildRequires:  pkgconfig(libnotify) pkgconfig(libopenjp2) pkgconfig(libpcre)
BuildRequires:  pkgconfig(libpng) pkgconfig(libseccomp) pkgconfig(libsecret-1)
BuildRequires:  pkgconfig(libsystemd) pkgconfig(libtasn1)
BuildRequires:  pkgconfig(libwebp) pkgconfig(libwoff2dec) pkgconfig(libxslt)
BuildRequires:  pkgconfig(sqlite3) pkgconfig(wayland-client) pkgconfig(wayland-egl)
BuildRequires:  pkgconfig(wayland-protocols) pkgconfig(wayland-server)
BuildRequires:  pkgconfig(wpe-1.0) pkgconfig(wpebackend-fdo-1.0) pkgconfig(xt)
BuildRequires:  pkgconfig(gbm) pkgconfig(libdrm) pkgconfig(libjxl) pkgconfig(epoxy)
BuildRequires:  pkgconfig(gstreamer-plugins-bad-1.0) unifdef
BuildRequires:  pkgconfig(libsoup-3.0)
%if %{with gamepad}	
BuildRequires:  pkgconfig(manette-0.2)
%endif
%if %{with docs}
BuildRequires:  gi-docgen
%endif
Requires:       javascriptcoregtk4.1 = %{version}-%{release}
Requires:       bubblewrap
Requires:       xdg-dbus-proxy
Recommends:     geoclue2
Recommends:     gstreamer1-plugins-bad-free
Recommends:     gstreamer1-plugins-good
Recommends:     xdg-desktop-portal-gtk
Provides:       bundled(angle)
Provides:       bundled(pdfjs)
Provides:       bundled(xdgmime)

%description
WebKitGTK is the port of the WebKit web rendering engine to the
GTK platform. This package contains WebKitGTK for GTK 3 and libsoup 3.

%package        devel
Summary:        Development files for webkit2gtk4.1
Requires:       webkit2gtk4.1 = %{version}-%{release}
Requires:       javascriptcoregtk4.1 = %{version}-%{release}
Requires:       javascriptcoregtk4.1-devel = %{version}-%{release}

%description    devel
The webkit2gtk4.1-devel package contains libraries, build data, and header
files for developing applications that use webkit2gtk4.1.

%package -n     javascriptcoregtk4.1
Summary:        JavaScript engine from webkit2gtk4.1

%description -n javascriptcoregtk4.1
This package contains JavaScript engine from webkit2gtk4.1.

%package -n     javascriptcoregtk4.1-devel
Summary:        Development files for JavaScript engine from webkit2gtk4.1
Requires:       javascriptcoregtk4.1 = %{version}-%{release}

%description -n javascriptcoregtk4.1-devel
The javascriptcoregtk4.1-devel package contains libraries, build data, and header
files for developing applications that use JavaScript engine from webkit2gtk-4.1.

%if %{with docs}
%package        doc
Summary:        Documentation files for webkit2gtk4.1
Requires:       webkit2gtk4.1 = %{version}-%{release}
BuildArch:      noarch

%description    doc
This package contains developer documentation for webkit2gtk4.1.
%endif

%prep
%autosetup -p1 -n %{upstream_name}-%{version}

%build
ncpus=%{_smp_build_ncpus}
maxcpus=$(( ($(awk '/^MemTotal:/{print $2}' /proc/meminfo)/1024/1024+1)/2*4/5))
if [ "$maxcpus" -ge 1 -a "$maxcpus" -lt "$ncpus" ]; then
    ncpus=$maxcpus
fi
%define _smp_mflags -j$ncpus

%ifarch aarch64
%global optflags %(echo %{optflags} | sed 's/-mbranch-protection=standard /-mbranch-protection=pac-ret /')
%endif

%define _vpath_builddir %{_vendor}-%{_target_os}-build/webkit2gtk-4.1
%cmake \
  -GNinja \
  -DPORT=GTK \
  -DCMAKE_BUILD_TYPE=Release \
  -DENABLE_WEBDRIVER=OFF \
%if %{without docs}
  -DENABLE_DOCUMENTATION=OFF \
%endif
%if %{without gamepad}
  -DENABLE_GAMEPAD=OFF \
%endif
%ifarch aarch64
  -DUSE_64KB_PAGE_BLOCK=ON \
%endif
  -DUSE_AVIF=OFF \
  %{nil}

%define _vpath_builddir %{_vendor}-%{_target_os}-build/webkit2gtk-4.1
export NINJA_STATUS="[webkit2gtk-4.1][%f/%t %es] "
%cmake_build

%install
%define _vpath_builddir %{_vendor}-%{_target_os}-build/webkit2gtk-4.1
%cmake_install
%find_lang WebKitGTK-4.1

%add_to_license_files Source/JavaScriptCore/COPYING.LIB
%add_to_license_files Source/ThirdParty/ANGLE/LICENSE
%add_to_license_files Source/ThirdParty/ANGLE/src/common/third_party/xxhash/LICENSE
%add_to_license_files Source/ThirdParty/ANGLE/src/third_party/libXNVCtrl/LICENSE
%add_to_license_files Source/WebCore/LICENSE-APPLE
%add_to_license_files Source/WebCore/LICENSE-LGPL-2
%add_to_license_files Source/WebCore/LICENSE-LGPL-2.1
%add_to_license_files Source/WebInspectorUI/UserInterface/External/CodeMirror/LICENSE
%add_to_license_files Source/WebInspectorUI/UserInterface/External/Esprima/LICENSE
%add_to_license_files Source/WebInspectorUI/UserInterface/External/three.js/LICENSE
%add_to_license_files Source/WTF/icu/LICENSE
%add_to_license_files Source/WTF/wtf/dtoa/COPYING
%add_to_license_files Source/WTF/wtf/dtoa/LICENSE

%files -f WebKitGTK-4.1.lang
%license _license_files/*ThirdParty*
%license _license_files/*WebCore*
%license _license_files/*WebInspectorUI*
%license _license_files/*WTF*
%{_libdir}/libwebkit2gtk-4.1.so.0*
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/WebKit2-4.1.typelib
%{_libdir}/girepository-1.0/WebKit2WebExtension-4.1.typelib
%{_libdir}/webkit2gtk-4.1/
%{_libexecdir}/webkit2gtk-4.1/
%exclude %{_libexecdir}/webkit2gtk-4.1/MiniBrowser
%exclude %{_libexecdir}/webkit2gtk-4.1/jsc

%files devel
%{_libexecdir}/webkit2gtk-4.1/MiniBrowser
%{_includedir}/webkitgtk-4.1/
%exclude %{_includedir}/webkitgtk-4.1/JavaScriptCore
%exclude %{_includedir}/webkitgtk-4.1/jsc
%{_libdir}/libwebkit2gtk-4.1.so
%{_libdir}/pkgconfig/webkit2gtk-4.1.pc
%{_libdir}/pkgconfig/webkit2gtk-web-extension-4.1.pc
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/WebKit2-4.1.gir
%{_datadir}/gir-1.0/WebKit2WebExtension-4.1.gir

%files -n javascriptcoregtk4.1
%license _license_files/*JavaScriptCore*
%{_libdir}/libjavascriptcoregtk-4.1.so.0*
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/JavaScriptCore-4.1.typelib

%files -n javascriptcoregtk4.1-devel
%{_libexecdir}/webkit2gtk-4.1/jsc
%dir %{_includedir}/webkitgtk-4.1
%{_includedir}/webkitgtk-4.1/JavaScriptCore/
%{_includedir}/webkitgtk-4.1/jsc/
%{_libdir}/libjavascriptcoregtk-4.1.so
%{_libdir}/pkgconfig/javascriptcoregtk-4.1.pc
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/JavaScriptCore-4.1.gir

%if %{with docs}
%files doc
%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html
%{_datadir}/gtk-doc/html/javascriptcoregtk-4.1/
%{_datadir}/gtk-doc/html/webkit2gtk-4.1/
%{_datadir}/gtk-doc/html/webkit2gtk-web-extension-4.1/
%endif

%changelog
* Fri Dec 20 2024 Rebuild Robot <rebot@opencloudos.org> - 2.41.6-9
- [Type] other
- [DESC] Rebuilt for icu

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.41.6-8
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.41.6-7
- Rebuilt for loongarch release

* Tue Apr 23 2024 Rebuild Robot <rebot@opencloudos.org> - 2.41.6-6
- Rebuilt for gstreamer1-plugins-bad-free

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.41.6-5
- Rebuilt for OpenCloudOS Stream 23.09

* Mon Aug 28 2023 Xiaojie Chen <jackxjchen@tencent.com> - 2.41.6-4
- Split into webkit2gtk4.1 from webkitgtk

* Wed Aug 23 2023 rockerzhu <rockerzhu@tencent.com> - 2.41.6-3
- Rebuilt for icu 73.2

* Fri Aug 18 2023 Wang Guodong <gordonwwang@tencent.com> - 2.41.6-2
- Rebuilt for enchant2 2.5.0

* Tue Jul 18 2023 Xiaojie Chen <jackxjchen@tencent.com> - 2.41.6-1
- Upgrade to upstream version 2.41.6

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.38.2-5
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.38.2-4
- Rebuilt for OpenCloudOS Stream 23

* Mon Feb 06 2023 Zhao Zhen <jeremiazhao@tencent.com> - 2.38.2-3
- Fixed bug of building error on arm64

* Wed Dec 07 2022 Xiaojie Chen <jackxjchen@tencent.com> - 2.38.2-2
- Enable gtk4 and libsoup3

* Mon Nov 21 2022 Xiaojie Chen <jackxjchen@tencent.com> - 2.38.2-1
- Initial build
